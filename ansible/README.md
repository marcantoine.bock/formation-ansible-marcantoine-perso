# Known issues
```
$ ansible -m ping all -i inventory 
10.0.1.184 | UNREACHABLE! => {
    "msg": "Failed to connect to the host via ssh: kex_exchange_identification: Connection closed by remote host\r\n",
    "unreachable": true
}
```
## Solution 
Before attempting the following, try to change the proxy command in ssh.cfg file (change bastion with its IP)
Add ssh key `$ ssh-add ssh-key` 
If you are using ubuntu on AWS EC2, execute `$ ssh-agent bash` before the ssh-add 

# Commands
```
$ ansible -m ping all -i inventory 
$ ansible-playbook -i inventory nginx.yaml
```
