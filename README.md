# Création de compte Aws

/!\ à revoir ! 

- Cliquer sur le lien suivant afin d'obtenir la création d'un compte aws pour une durée de 3 mois maximum [https://apps.powerapps.com/play/d0dd8d9a-0bef-425a-bb1f-ee9c630e09e4?tenantId=5de96c96-c87c-4dce-aad9-f5c557b52ac1&hint=43ef1c8c-9042-400d-822a-6c0e352b09c1&source=businessAppDiscovery](https://apps.powerapps.com/play/d0dd8d9a-0bef-425a-bb1f-ee9c630e09e4?tenantId=5de96c96-c87c-4dce-aad9-f5c557b52ac1&hint=43ef1c8c-9042-400d-822a-6c0e352b09c1&source=businessAppDiscovery)

- Sélectionner une date de fin inférieur à 3 mois maximum après la date de début de la création du compte AWS puis cliquer sur Submit.

![requete pour creation de compte](images/date_creation_compte.png)

- Après avoir cliqué sur Submit, vous allez recevoir deux mails. Cliquer sur le bouton "GO AWS" comme dans l’image ci -dessous.

![email go to AWS](images/email_go_to_aws.png)

- Dans la barre de recherche, taper « users », puis cliquer sur utilisateurs "Fonctionnalité IAM"

![users search bar](images/users_search_bar.png)

- Cliquer ensuite sur le bouton "ajouter des utilisateurs" 

![add users bouton](images/add_users_bouton.png)

- Dans le nom d’utilisateur renseigner votre adresse mail wavestone. Puis cocher les cases clé d’accès et mot de passe. Puis cliquer sur suivant :

![Define users details](images/Define_users_details.png)

- Cliquer sur "attacher directement les stratégies existantes", cocher la case avec le rôle "AdministratorAccess" puis cliquer sur suivant :

![AdministratorAccess role](images/AdministratorAccess.png)

- Ajouter une clé Formation et une valeur Ansible, puis cliquer sur suivant :

![balise Formation Ansible](images/balise.png)

- Assurez vous que vous avez mis la bonne stratégie : "AdministratorAccess" puis cliquer sur créer un utilisateur.

![Etape vérification](images/verification.png)


- Cliquer sur "envoyez un email" et enregistrer bien vos ID de clé d’accès et votre clé d’accès secrète ainsi que votre mot de passe. Vous pouvez soit télécharger le .csv ou bien écrire vos clés et mot de passe dans un bloc note ou autre fichier texte. Attention ne divulguer pas vos identifiants secrets.

![informations utilisateurs](images/informations_utilisateurs.png)

- Dans le mail que vous allez recevoir, vous allez récupérer un url qui va vous permettre de vous connecter à votre compte aws. Par exemple, mon url de connexion est le suivant : https://aws-account-ida-23.signin.aws.amazon.com/console

- Copier coller votre url dans votre navigateur de rechercher. Vous apparaissez sur la page suivante. Il faut renseigner votre nom d’utilisateur qui est votre adresse mail Wavestone ainsi que votre mot de passe qui vous a été donné leur de la création de votre compte (vous l'avez sauvegarder soit sur le fichier csv soit sur un bloc note)

![connexion compte aws](images/connexion_compte_aws.png)

- Taper votre ancien mot de passe et créer en un nouveau.

![changement de mot de passe](images/changement_de_mot_de_passe.png)

- Félicitations, votre compte est désormais créer avec un mot de passe que vous avez définis. Vous pouvez maintenant déployer ou utiliser des instances AWS. Assurer vous d’être bien dans la région Irlande. Si ce n’est pas le cas sélectionner Irlande en cliquer dessus.

![page accueil aws](images/page_accueil_aws_region.png)

+ Création d'un bucket ! 

# Déploiement d'une machine de travail

## Fork du gitlab

## Ajout des variables gitlab

## Modifier le code là ou c'est nécessaire (ex. bucket name)

# Déploiement des machines Ansible

## Connexion à la machine de travail via SSM

## Déploiement des ressources 
/!\ attention à ne pas détruire les ressources via la pipeline gitlab avant de détruire les ressources infra ansible
cd /home/ssm-user/formation-ansible/terraform/ansible_vms
vi provider.tf
remplir provider avec ...

## Destruction des ressources 
/!\ ordre important
