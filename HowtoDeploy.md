# How to Deploy "formation-ansible" project v2

## Requierements :
- Visual studio code and git 

## Steps before deployment
In virtual studio code, open a bash terminal: 
- git clone https://gitlab.com/mehdi.bettiche/formation-ansible.git

Before deploying the project with terraform, you have fews steps to achieve: 
Create provider.tf with your aws credentials and specify the region like the code below:
- cd formation-ansible/terraform/work_vm
- vi provider.tf

provider "aws" {
  region     = "eu-west-1"
  access_key = "your_access-key"
  secret_key = "your_secret-key"
}

- In AWS interface, go to "paire de clés" or "key pair" and create a key pair named : ansible_key with rsa encryption and .pem format
To be sure you get and download your ansible key, check with this two command in your terminal.
- cd 
- cat Downloads/ansible_key.pem 
You should see your secret key

In the bash terminal, you will deploy the project with terraform : 
- cd formation-ansible/terraform/work_vm
- Terraform init
- Terraform plan
- Terraform apply
If the terminal show a message, "enter a value:", write yes 

After these commands, you will have an output with different ip address. I suggest you to copy this output. It will be useful later.

## Connection to work vm
When the terraform deployment is finished, we will connect in ssh on work vm machine
- ssh -i ~/Downloads/ansible_key.pem ubuntu@"ip address of your work vm from the terraform output"
- Example, you just have to change the ip address with your: ssh -i ~/Downloads/ansible_key.pem ubuntu@34.247.175.18
After this command you are now connect on your work_vm

## Create the key pair file : devops.pem 
It’s not the good way to do this but we proceed like that for now. This part will be change in configuration code
Open a new bash terminal:
- cd 
- cat ~/Downloads/ansible_key.pem
- copy your key

Create and copy paste the key, in terminal where you are connnecting on the vm_work vm:
- vi ~/devops.pem
- Paste the key inside file
- wq! For save and quit

## Second terraform deployment but inside the vm work
Attention peut etre que le git clone ne fonctionne pas ?
git clone https://gitlab.com/mehdi.bettiche/formation-ansible.git

You are connecting on the vm_works, go terraform folder and inside ansible_vms folder,
- cd formation-ansible/terraform/ansible_vms

Create provider.tf with your credentials and specify the region like the code below:
- vi profider.tf

provider "aws" {
  region     = "eu-west-1"
  access_key = "your_access-key"
  secret_key = "your_secret-key"
}
In the bash terminal, you will deploy the project with terraform : 
- cd formation-ansible/terraform/ansible_vms (Normally, you are already here)
- Terraform init
- Terraform plan
- Terraform apply

# Dynamic Inventory 
In the current configuration we have ansible, terraform and python3 already install

In order to use the Aws plugin we have to install boto3.
- pip install boto3

If you don’t have python3 or boto3 installed it will give the following error
ERROR! The ec2 dynamic inventory plugin requires boto3 and botocore.

To use the plugin aws_ec2 there are different solution to authentificate with AWS, for example there are IAM role authentification, AWS cli and environment variable who specify your credentials. I will describe the CLI method here.

## Install CLI AWS
 - sudo apt install unzip
 - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
 - unzip awscliv2.zip
 - sudo ./aws/install
 - aws configure
 Enter your access key, secret key and default region default eu-west-1, the output format is json by default you can click on enter

## Create folder and yaml file that will contain the configuration file of dynamic inventory
- mkdir -p ~/formation-ansible/ansible/dynamic_inventory_inventory
- cd ~/formation-ansible/ansible/dynamic_inventory_inventory
- vi aws_ec2.yaml

Copy paste the code below, be careful of the indentation :
---
plugin: aws_ec2
boto_profile: default
regions:
  - eu-west-1
filters:
   instance-state-name: running
strict: False
keyed_groups:
  - prefix: tag
    key: tags
compose:
  ansible_host: private_ip_address

## Modifying ansible.cfg
In formation-ansible/ansible/ansible.cfg,
- cd 
- cd formation-ansible/ansible/
- vi ansible.cfg
Click on i to be in insert mode and copy paste the code below after the code which is already here:
[inventory]  
enable_plugins = aws_ec2
[defaults]
inventory = ~/formation-ansible/ansible/dynamic_inventory_inventory/aws_ec2.yaml

## List all instance
Commande qui retourne l’ensemble des paramètres des instances ec2 en format json
ansible-inventory -i ~/formation-ansible/ansible/dynamic_inventory_inventory --list
or can use thanks to defaults part in ansible.cfg, that's work here formation-ansible/ansible : ansible-inventory --list

## Command to group all instance
Commande pour lister les groupes dynamiques
ansible-inventory -i ~/formation-ansible/ansible/dynamic_inventory_inventory --graph
or can use thanks to defaults part in ansible.cfg, that's work here formation-ansible/ansible  : ansible-inventory --graph

## Command to ping all instance
Commande ping l’ensemble des instances 
- chmod 400 ~/devops.pem
- ssh-agent bash
- ssh-add ~/devops.pem 
- ansible all -m ping -i ~/formation-ansible/ansible/dynamic_inventory_inventory
or default : ansible -m ping all -i inventory

