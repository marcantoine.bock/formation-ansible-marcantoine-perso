module "main"{
    source = "./infrastructure"
    liste_participants = var.liste_participants
}

output "liste_vms" {
  value = module.main
}
