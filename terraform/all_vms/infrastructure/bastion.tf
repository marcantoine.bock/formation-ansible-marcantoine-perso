
################################################################################
# Compute Resources
################################################################################
resource "aws_instance" "bastion" {
  for_each = var.liste_participants
  ami                         = "ami-08edbb0e85d6a0a07" # UBUNTU # "ami-04dd4500af104442f" # AWS-AMI # data.aws_ami.ubuntu.id
  subnet_id                   = aws_subnet.devops_admin_subnet.id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  security_groups             = [aws_security_group.bastion_sg.id]
  key_name                    = local.key_name
  
  tags = {
    Name = "bastion_vm_${each.key}"
  }
}

################################################################################
# Network Resources
################################################################################

resource "aws_route_table" "devops_bastion_rt" {
  vpc_id = aws_vpc.devops_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.devops_gw.id
  }

  tags = {
    Name = "devops_rt"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.devops_admin_subnet.id
  route_table_id = aws_route_table.devops_rt.id
}

resource "aws_subnet" "devops_admin_subnet" {
  vpc_id     = aws_vpc.devops_vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "devops_admin_subnet"
  }
}

resource "aws_security_group" "bastion_sg" {
  name   = "bastion_sg"
  vpc_id = aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
