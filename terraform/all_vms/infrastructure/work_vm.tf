
################################################################################
# Compute Resources
################################################################################
resource "aws_instance" "work" {
  for_each = var.liste_participants
  ami                         = "ami-08edbb0e85d6a0a07" # UBUNTU # "ami-04dd4500af104442f" # AWS-AMI # data.aws_ami.ubuntu.id
  subnet_id                   = aws_subnet.devops_admin_subnet.id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  security_groups             = [aws_security_group.work_sg.id]
  key_name                    = local.key_name
  
  user_data = file("${path.module}/../../../install/install.sh")

  tags = {
    Name = "work_vm_${each.key}"
  }
}

################################################################################
# Network Resources
################################################################################

resource "aws_security_group" "work_sg" {
  name   = "work_sg"
  vpc_id = aws_vpc.devops_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}